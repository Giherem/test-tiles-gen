#include "TilingProblem.h"

TilingProblem::TilingProblem(int sx, int sy) : Space(), allVvars(*this, sx * sy, 0, 15), xSize(sx), ySize(sy), rnd(time(nullptr))
{
    auto vars= getVarsMatrix();
    TupleSet tv(2);
    TupleSet th(2);
    for (int i: {0, 1, 2, 3, 8, 9, 10, 11})         // Ces tiles n'ont aucun chemin qui part en bas
        for (int j: {0, 2, 4, 6, 8, 10, 12, 14})    // Ces tiles n'ont aucun chemin qui part en haut
            tv.add({i,j});
    for (int i: {4, 5, 6, 7, 12, 13, 14, 15})       // Ces tiles ont un chemin qui part en bas
        for (int j: {1, 3, 5, 7, 9, 11, 13, 15})    // Ces tiles ont un chemin qui part en haut
            tv.add({i,j});

    for (int i: {0, 1, 4, 5, 8, 9, 12, 13})         // Ces tiles n'ont aucun chemin qui part à droite
        for (int j: {0, 1, 2, 3, 4, 5, 6, 7})       // Ces tiles n'ont aucun chemin qui part à gauche
            th.add({i,j});
    for (int i: {2, 3, 6, 7, 10, 11, 14, 15})       // Ces tiles ont un chemin qui part à droite
        for (int j: {8, 9, 10, 11, 12, 13, 14, 15}) // Ces tiles ont un chemin qui partà gauche
            th.add({i,j});

    tv.finalize();
    th.finalize();

    for (int x=0;x<xSize;++x)
        for (int y=0;y<ySize;++y)
        {
            if (x != xSize-1)
            {extensional(*this,IntVarArgs{vars[x][y],vars[x+1][y]},th);}
            // on pose les contraintes verticales
            if (y != ySize-1)
            {extensional(*this,IntVarArgs{vars[x][y],vars[x][y+1]},tv);}
            rel(*this,vars[x][y] != 1 );
            rel(*this,vars[x][y] != 2 );
            rel(*this,vars[x][y] != 4 );
            rel(*this,vars[x][y] != 8 );
        }

    branch(*this,allVvars,INT_VAR_SIZE_MIN(), INT_VAL_RND(rnd));
}

std::vector<std::vector<IntVar>> TilingProblem::getVarsMatrix() const
{
    std::vector<std::vector<IntVar>> vars;
    vars.reserve(xSize);
    int cpt=0;
    for (int i=0;i<xSize;++i)
    {
        vars.emplace_back();
        vars.back().reserve(ySize);
        for (int j=0;j<ySize;++j)
            vars.back().push_back(allVvars[cpt++]);
    }
    return vars;
}

Space *TilingProblem::copy()
{
    return new TilingProblem(*this);
}
