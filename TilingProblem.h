#pragma once
#include <gecode/minimodel.hh>
#include <vector>
#include <ctime>

using namespace Gecode;
class TilingProblem : public Space
{
protected:
    IntVarArray allVvars;
    Rnd rnd;
    int xSize,ySize;

public:
    TilingProblem(int sx, int sy);
    TilingProblem(TilingProblem& src) : Space(src), rnd(time(nullptr))
    {

        allVvars.update(*this,src.allVvars);
        xSize = src.xSize;
        ySize = src.ySize;
    }

    Space *copy() override;

    std::vector<std::vector<IntVar>> getVarsMatrix() const;
};
