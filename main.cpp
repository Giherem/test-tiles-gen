#include <iostream>
#include <SFML/Graphics.hpp>
#include "Tilemap.h"
#include "TilingProblem.h"
#include <vector>

using std::vector;
using std::cout;
using std::endl;


int main()
{
    srand(std::time(nullptr));

    // Taille d'une tile en pixels dnas le fichier image qui les regroupe
    int tileSize = 16;
    int xSize = 96, ySize = 64;
    // On définit la tialle de la fenêtre en fnction de la taille du damier qu'on va générer
    int windowWidth = xSize * tileSize;
    int windowHeight = ySize * tileSize;
    sf::RenderWindow window(sf::VideoMode(windowWidth, windowHeight), "Tilemap");

    auto prob = new TilingProblem(xSize, ySize);
    DFS<TilingProblem> searchEngine(prob);
    auto solution = searchEngine.next();
    auto varsSol = solution->getVarsMatrix();
    auto stats = searchEngine.statistics();
    cout <<
         "depth : "<<stats.depth<<endl<<
         "fails : "<<stats.fail<<endl<<
         "nodes : "<<stats.node<<endl<<
         "props : "<<stats.propagate<<endl;


    // Le vecteur de (numéros de) tiles qui sera passé à l'affichage
    vector<int> lvl(xSize * ySize);
    // On le remplit avec les valeurs des variables. À ce niveau, le problème est résolu donc elles sont toutes
    // censées avoir une unique valeur possible (i.e. avoir été affectées)
    int idxtile = 0;
        for (int y = 0; y < ySize;++y)
            for (int x = 0;x < xSize;++x)
        {
            auto& var = varsSol[x][y];
            lvl[idxtile] = var.val();
            ++idxtile;
        }
    // create the tilemap from the level definition
    TileMap map;
    if (!map.load("tileset.png", sf::Vector2u(tileSize, tileSize), lvl.data(), xSize, ySize))
        return -1;

//    window.setVerticalSyncEnabled(true);
    window.setFramerateLimit(100);
    // run the main loop
    while (window.isOpen())
    {
        // handle events
        sf::Event event{};
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        // draw the map
        window.clear();
        window.draw(map);
        window.display();
    }

    return 0;
}
